﻿using MPThreept.utils;
using MPThreept.utils.algorithms;
using MPThreept.utils.interfaces;
using System;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace MPThreept
{
    public partial class FileForm : Form
    {
        private string path;
        private ICryptoAlgorithm cryptoAlgorithm;

        public FileForm()
        {
            this.cryptoAlgorithm = new AESAlgorithm();
            InitializeComponent();
        }

        /*----------------------- Обработчики событий -----------------------*/

        private void OpenButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog.ShowDialog();
            path = OpenFileDialog.FileName;
            PathTextBox.Text = OpenFileDialog.FileName;
        }

        private void EncryptButton_Click(object sender, EventArgs e)
        {          
            var key = KeyTextBox.Text.Trim();
            if (key.Equals(""))
            {
                ErrorLabel.Text = "Введите значение пароля";
                return;
            }
            var keyBytes = Encoding.UTF8.GetBytes(key.ToCharArray());
            var keyData = GetSha256Hash(keyBytes);

            SourceFile file = new SourceFile(path); 

            byte[] IVBytes = new byte[16];
            Array.Copy(GetSha256Hash(keyData), IVBytes, 16);

            try
            {
                var encrypted = cryptoAlgorithm.Encrypt(file.Data, IVBytes, keyData);
                file.SaveAs(encrypted, Mode.ENCRTYPTED);
                ClearFields();
                WriteSuccess("Успешно зашифровано!");
            } catch (Exception)
            {
                ClearFields();
                ErrorLabel.Text = "Ошибка шифрования";
            }
            
        }

        private void DecryptButton_Click(object sender, EventArgs e)
        {
            var key = KeyTextBox.Text.Trim();
            if (key.Equals(""))
            {
                WriteError("Введите значение пароля");
                return;
            }
            var keyBytes = Encoding.UTF8.GetBytes(key.ToCharArray());
            var keyData = GetSha256Hash(keyBytes);

            SourceFile file = new SourceFile(path);

            byte[] IVBytes = new byte[16];
            Array.Copy(GetSha256Hash(keyData), IVBytes, 16);

            try
            {
                var decrypted = cryptoAlgorithm.Decrypt(file.Data, IVBytes, keyData);
                file.SaveAs(decrypted, Mode.DECRYPTED);
                ClearFields();
                WriteSuccess("Успешно расшифровано!");
            }
            catch (Exception)
            {
                ClearFields();
                WriteError("Ошибка дешифровки");
            }
        }


        /*----------------------- Служебные функции -----------------------*/

        private byte[] GetSha256Hash(byte[] data)
        {
            using (var SHA = new SHA256Cng())
            {
                var result = SHA.ComputeHash(data);
                return result;
            }
        }

        private void ClearFields()
        {
            KeyTextBox.Text = "";
            PathTextBox.Text = "";
        }

        private void WriteError(string message)
        {
            ErrorLabel.ForeColor = Color.Red;
            ErrorLabel.Text = message;
        }

        private void WriteSuccess(string message)
        {
            ErrorLabel.ForeColor = Color.Green;
            ErrorLabel.Text = message;
        }
    }

    
}
