﻿using MPThreept.utils.interfaces;
using System.IO;
using System.Security.Cryptography;

namespace MPThreept.utils.algorithms
{
    class AESAlgorithm : ICryptoAlgorithm
    {
        public byte[] Decrypt(byte[] file, byte[] IV, byte[] key)
        {
            byte[] decrypted;

            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = IV;

                var decryptor = aesAlg.CreateDecryptor();
                decrypted = decryptor.TransformFinalBlock(file, 0, file.Length);
            }

            return decrypted;
        }

        public byte[] Encrypt(byte[] file, byte[] IV, byte[] key)
        {
            byte[] encrypted;

            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = key;
                aesAlg.IV = IV;

                var encryptor = aesAlg.CreateEncryptor();
                encrypted = encryptor.TransformFinalBlock(file, 0, file.Length);
            }

            return encrypted;
        }
    }
}
