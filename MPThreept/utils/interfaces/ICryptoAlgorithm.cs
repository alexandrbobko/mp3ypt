﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MPThreept.utils.interfaces
{
    interface ICryptoAlgorithm
    {
        byte[] Encrypt(byte[] file, byte[] IV, byte[] key);
        byte[] Decrypt(byte[] file, byte[] IV, byte[] key);
    }
}
