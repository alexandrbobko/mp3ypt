﻿using System.IO;

namespace MPThreept.utils
{
    class SourceFile
    {
        public string Directory { get; }
        public string FileName { get; }
        public string Extension { get; }
        public int Length { get; }
        public byte[] Data { get; }

        public SourceFile(string path)
        {
            Directory = path;
            using (var file = File.OpenRead(path))
            {
                Length = (int)file.Length;
                Extension = Path.GetExtension(path);
                FileName = Path.GetFileNameWithoutExtension(path);
                Directory = Path.GetDirectoryName(path);
                Data = new byte[Length];

                file.Read(Data, 0, Length);
            }
        }

        public void SaveAs(byte[] fileData, Mode mode)
        {
            var fileName = FileName;
            if (mode == Mode.ENCRTYPTED)
                fileName += "enc";
            fileName += Extension;

            var filePath = Path.Combine(Directory, fileName);

            using (var file = File.Create(filePath))
            {
                file.Write(fileData, 0, fileData.Length);
            }
        }
    }

    public enum Mode
    {
        ENCRTYPTED,
        DECRYPTED
    }
}
